# This Dockerfile is used to build an image based on Ubuntu VNC accessible machine

FROM ubuntu:16.04
MAINTAINER Guillem Hernandez Sola "guillem@itnove.com"
ENV REFRESHED_AT 2016-10-05
ENV JAVA_HOME /usr/lib/jvm/java-8-openjdk-amd64
ENV DEBIAN_FRONTEND noninteractive
ENV DISPLAY :1
ENV VNC_COL_DEPTH 24
ENV VNC_RESOLUTION 1280x1024
ENV VNC_PW vncpassword
ENV M2_HOME /usr/share/maven
ENV MAVEN_HOME /usr/share/maven

############### Install xvnc / xfce installation
RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get autoclean
RUN apt-get install -y supervisor xfce4 vnc4server wget curl mysql-server
### Install via apt-get various packages
RUN apt-get install -y xfce4-terminal gedit libxslt1.1 openjdk-8-jdk xvfb gradle
RUN apt-get install -y software-properties-common git libxext-dev libxrender-dev
RUN apt-get install -y libxtst-dev libgtk2.0-0 libcanberra-gtk-module unzip maven sudo

############### Install firefox
RUN apt-get install -y firefox

############### Install Intellij via apt-get
RUN add-apt-repository ppa:mmk2410/intellij-idea-community
RUN apt-get update
RUN apt-get install -y intellij-idea-community

# xvnc server port, if $DISPLAY=:1 port will be 5901
EXPOSE 5901

ADD .vnc /root/.vnc
ADD .config /root/.config
ADD scripts /root/scripts
ADD Desktop /root/Desktop
RUN chmod +x /root/.vnc/xstartup /etc/X11/xinit/xinitrc /root/scripts/*.sh
RUN chmod -R 777 /root/Desktop

# Set the time zone
ENV TZ=Europe/Andorra
RUN echo $TZ | tee /etc/timezone
RUN dpkg-reconfigure --frontend noninteractive tzdata

ENTRYPOINT ["/root/scripts/vnc_startup.sh"]
CMD ["--tail-log"]
