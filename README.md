# Ubuntu XFCE 4 Docker container with "headless" VNC session

The repository contains a collection of Docker images with headless VNC environments.

Each docker image is installed with the following components:

* Desktop environment [**Xfce4**](http://www.xfce.org)
* VNC-Server (default VNC port `5901`)
* Java OpenJDK 8
* Mozilla Firefox
* Google Chrome
* Intellij 16.2.3
* Git
* Maven

### Current provided OS & UI sessions:
* __Ubuntu 16.04 with `Xfce4` UI session:__

  Download the docker image from Docker Hub

      docker pull itnove/fib-gps

  Run command with mapping to local port `5901`:

      docker run -d -p 5901:5901 docker pull itnove/fib-gps

  => connect via __VNC viewer `localhost:5901`__, default password: `vncpassword`

### Contact
For questions or maybe some hints, feel free to contact us via **[guillem@itnove.com](mailto:guillem@itnove.com)**.

The guys behind:

**ITNove**

*Passeig de Gràcia 110, 4rt 2a, 08008 Barcelona*

*Tel. +34-679-977187*

*Homepage: http://www.itnove.com*
